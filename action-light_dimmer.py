#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import configparser
from hermes_python.hermes import Hermes
from hermes_python.ffi.utils import MqttOptions
from hermes_python.ontology import *
import requests             #for rest api acess to openhab
from colour import Color    #for getting rgb values for the colors 
import colorsys             #for converting rgb to hsv/b values
import io
openhab_answer = "<Response [202]>"
translation_table = {       #translation table to replace german umlauts
    "ä": "ae",
    "ö": "oe",
    "ü": "ue",
    "Ü": "Ue",
    "Ö": "Oe",
    "Ä": "Ae"
}
CONFIGURATION_ENCODING_FORMAT = "utf-8"
CONFIG_INI = "config.ini"

class SnipsConfigParser(configparser.SafeConfigParser):
    def to_dict(self):
        return {section : {option_name : option for option_name, option in self.items(section)} for section in self.sections()}

def read_configuration_file(configuration_file):
    try:
        with io.open(configuration_file, encoding=CONFIGURATION_ENCODING_FORMAT) as f:
            conf_parser = SnipsConfigParser()
            conf_parser.readfp(f)
            return conf_parser.to_dict()
    except (IOError, configparser.Error) as e:
        return dict()

def subscribe_intent_callback(hermes, intentMessage):
    conf = read_configuration_file(CONFIG_INI)
    action_wrapper(hermes, intentMessage, conf)


def action_wrapper(hermes, intentMessage, conf):
    openhab_port = conf['global'].get("openhab_server_port")    #getting openhab port from config.ini
    openhab_server = conf['global'].get("openhab_server_url")   #getting openhab server ip from config.ini
    color_intent = intentMessage.slots.color.first().value      #getting color slot
    c = Color(color_intent)                                     #gets rgb values for slotvalue (for example red: 1, 0, 0)
    color_r, sat_r, bright_r = colorsys.rgb_to_hsv(c.red, c.green, c.blue)  #gets hsv/b value from r, g and b value
    color = 360 * color_r                                                   #upscaling color value
    sat = 100 * sat_r                                                       #upscaling saturation value
    bright = 100 * bright_r                                                 #upscaling brightness value
    data = color, sat, bright       
    data_s = str(data)      
    data_ss = data_s.replace("(","")
    data_sss = data_ss.replace(")","")
    room = intentMessage.slots.deviceLocation.first().value                 #gets deviceLocation slot as room
    room_without = room.translate(str.maketrans(translation_table))         #removes german umlauts
    room_color = room_without
    room_color += "_color"
    response = requests.put('http://{}:{}/rest/items/{}/state'.format(openhab_server, openhab_port, room_color), data_sss)     #sending color value to the openhab rest api
    if str(response) == openhab_answer:
        result_sentence = "Wechsle das Licht in {}".format(str(room))   #define result sentence
    else:
        result_sentence = "Der Raum {} ist nicht definiert. Bitte erstelle in OpenHAB ein Eitem mit dem Namen , : {}, unterstrich, caler. ".format(str(room),str(room))
    
    current_session_id = intentMessage.session_id
    hermes.publish_end_session(current_session_id, result_sentence)

if __name__ == "__main__":
    conf = read_configuration_file(CONFIG_INI)
    mqtt_server = conf['global'].get("mqtt_server")
    mqtt_opts = MqttOptions()
    with Hermes(mqtt_server) as h:
        h.subscribe_intent("Paule:FarbeWechseln", subscribe_intent_callback) \
.start()
